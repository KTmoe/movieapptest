package com.example.movieapptest.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.movieapptest.data.converters.*
import com.example.movieapptest.data.dao.MovieDao
import com.example.movieapptest.data.dao.MovieDetailDao
import com.example.movieapptest.data.model.Movie
import com.example.movieapptest.data.model.SpokenLanguage
import com.example.movieapptest.data.network.responses.MovieDetailResponse

@Database(entities = [Movie::class, MovieDetailResponse::class], version = 1, exportSchema = false)
@TypeConverters(GenreConverter::class, ConvertFromGenre::class, ConvertFromCompany::class, ConvertFromCountry::class, ConvertFromLanguage::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun movieDao() : MovieDao

    abstract fun movieDetailDao() : MovieDetailDao

    companion object {

        private var INSTANCE : AppDatabase? = null

        fun getInstance(context: Context) : AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, "MOVIE.DB")
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE!!
        }

        fun destoryDatabase() {
            INSTANCE = null
        }

    }

}