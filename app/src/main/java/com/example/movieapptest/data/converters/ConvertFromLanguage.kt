package com.example.movieapptest.data.converters

import androidx.room.TypeConverter
import com.example.movieapptest.data.model.Genre
import com.example.movieapptest.data.model.SpokenLanguage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ConvertFromLanguage {
    @TypeConverter
    fun toString(list: List<SpokenLanguage>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toList(value: String): List<SpokenLanguage> {
        val listType = object : TypeToken<List<SpokenLanguage>>() {}.type
        return Gson().fromJson(value, listType)
    }

}