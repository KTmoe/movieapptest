package com.example.movieapptest.data.datasource

import com.example.movieapptest.data.network.responses.MovieDetailResponse

interface MovieDetailDataSource{

    fun save(movieDetail : MovieDetailResponse)

    fun get(movieid: Int) : MovieDetailResponse?

    fun delete()

}