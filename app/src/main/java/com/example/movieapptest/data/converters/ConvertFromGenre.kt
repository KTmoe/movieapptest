package com.example.movieapptest.data.converters

import androidx.room.TypeConverter
import com.example.movieapptest.data.model.Genre
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ConvertFromGenre {

    @TypeConverter
    fun toString(list: List<Genre>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toList(value: String): List<Genre> {
        val listType = object : TypeToken<List<Genre>>() {}.type
        return Gson().fromJson(value, listType)
    }

}