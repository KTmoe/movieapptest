package com.example.movieapptest.data.converters

import androidx.room.TypeConverter
import com.example.movieapptest.data.model.Genre
import com.example.movieapptest.data.model.ProductionCompany
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ConvertFromCompany {

    @TypeConverter
    fun toString(list: List<ProductionCompany>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toList(value: String): List<ProductionCompany> {
        val listType = object : TypeToken<List<ProductionCompany>>() {}.type
        return Gson().fromJson(value, listType)
    }
}