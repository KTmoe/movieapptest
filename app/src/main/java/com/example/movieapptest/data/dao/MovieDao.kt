package com.example.movieapptest.data.dao

import androidx.room.*
import com.example.movieapptest.data.model.Movie

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(movie : Movie) : Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(list: List<Movie>) : LongArray

    @Query("select * from movie")
    fun get() : MutableList<Movie>?

    @Query("select * from movie where id = :id")
    fun get(id : Int) : Movie?

    @Query("delete from movie")
    fun delete()

    @Query("delete from movie where id = :id")
    fun delete(id : Int)

}