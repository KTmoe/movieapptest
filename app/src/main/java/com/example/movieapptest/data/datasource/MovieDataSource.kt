package com.example.movieapptest.data.datasource

import com.example.movieapptest.data.model.Movie

interface MovieDataSource {

    fun save(list: List<Movie>)

    fun get() : MutableList<Movie>?

    fun delete()

}