package com.example.movieapptest.data.converters

import androidx.room.TypeConverter
import com.example.movieapptest.data.model.Genre
import com.example.movieapptest.data.model.ProductionCountry
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ConvertFromCountry {

    @TypeConverter
    fun toString(list: List<ProductionCountry>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun toList(value: String): List<ProductionCountry> {
        val listType = object : TypeToken<List<ProductionCountry>>() {}.type
        return Gson().fromJson(value, listType)
    }

}