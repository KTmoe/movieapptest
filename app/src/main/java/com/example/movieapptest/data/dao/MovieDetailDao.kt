package com.example.movieapptest.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movieapptest.data.network.responses.MovieDetailResponse

@Dao
interface MovieDetailDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(movieDetail : MovieDetailResponse) : Long

    @Query("select * from movie_detail where id = :id")
    fun get(id : Int) : MovieDetailResponse?

    @Query("Delete from movie_detail")
    fun delete()

}