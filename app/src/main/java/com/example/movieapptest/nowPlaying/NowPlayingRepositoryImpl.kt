package com.example.movieapptest.nowPlaying

import com.example.movieapptest.data.datasource.MovieDataSource
import com.example.movieapptest.data.db.AppDatabase
import com.example.movieapptest.data.model.Movie
import com.example.movieapptest.data.network.services.MovieService
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class NowPlayingRepositoryImpl(private val service : MovieService,
                               private val db : AppDatabase) : NowPlayingRepository, MovieDataSource {

    override fun save(list: List<Movie>) {
        db.movieDao().save(list)
    }

    override fun get(): MutableList<Movie>? {
        return db.movieDao().get()
    }

    override fun delete() {
        db.movieDao().delete()
    }

    override fun loadNowPlaying(): Observable<NowPlayingViewState> = service.loadNowPlaying()
        .subscribeOn(Schedulers.io())
        .map<NowPlayingViewState> {
            delete()
            save(it.results)
            NowPlayingViewState.SuccessNowPlayingListState(get() ?: emptyList())
        }
        .startWith( NowPlayingViewState.Loading )
        .onErrorReturn {
            if (get().isNullOrEmpty()) {
                NowPlayingViewState.FailNowPlayingListState(it.localizedMessage ?: "Unknown Error.")
            } else {
                NowPlayingViewState.SuccessNowPlayingListState(get() ?: emptyList())
            }

        }
}