package com.example.movieapptest.topRated

import com.example.movieapptest.data.datasource.MovieDataSource
import com.example.movieapptest.data.db.AppDatabase
import com.example.movieapptest.data.model.Movie
import com.example.movieapptest.data.network.services.MovieService
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class TopRatedRepositoryImpl(private val service: MovieService,
                             private val db : AppDatabase) : TopRatedRepository, MovieDataSource {
    override fun save(list: List<Movie>) {
        db.movieDao().save(list)
    }

    override fun get(): MutableList<Movie>? = db.movieDao().get()

    override fun delete() {
        db.movieDao().delete()
    }

    override fun loadTopRated(): Observable<TopRatedViewState> = service.loadTopRated()
        .subscribeOn(Schedulers.io())
        .map<TopRatedViewState> {
            delete()
            save(it.results)
            TopRatedViewState.SuccessTopRatedListState(get() ?: emptyList())
        }
        .startWith(TopRatedViewState.Loading)
        .onErrorReturn {

            if(get().isNullOrEmpty()){
                TopRatedViewState.FailTopRatedListState(it.localizedMessage ?: "Unknown Error.")
            }else {
                TopRatedViewState.SuccessTopRatedListState(get() ?: emptyList())
            }

        }
}