package com.example.movieapptest.movieDetail

import com.example.movieapptest.data.datasource.MovieDetailDataSource
import com.example.movieapptest.data.db.AppDatabase
import com.example.movieapptest.data.network.responses.MovieDetailResponse
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class MovieDetailRepositoryImpl(private val service: MovieDetailService,
                                private val db : AppDatabase) : MovieDetailRepository, MovieDetailDataSource {
    override fun delete() {
        db.movieDetailDao().delete()
    }

    override fun get(movieid: Int): MovieDetailResponse? = db.movieDetailDao().get(movieid)

    override fun save(movieDetail: MovieDetailResponse) {
        db.movieDetailDao().save(movieDetail)
    }

    override fun loadMovieDetail(movieId : String): Observable<MovieDetailViewState> = service.loadMovieDetail(movieId)
        .subscribeOn(Schedulers.io())
        .map <MovieDetailViewState> {
            delete()
            save(it)
            MovieDetailViewState.SuccessMovieDetailState(get(movieId.toInt()))
        }
        .startWith(MovieDetailViewState.LoadingMovieDetailState)
        .onErrorReturn {
            if(get(movieId.toInt()) == null){
                MovieDetailViewState.FailMovieDetailState(it.localizedMessage)
            }else {
                MovieDetailViewState.SuccessMovieDetailState(get(movieId.toInt()))
            }
        }

}