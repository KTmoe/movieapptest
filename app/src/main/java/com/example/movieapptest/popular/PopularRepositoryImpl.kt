package com.example.movieapptest.popular

import com.example.movieapptest.data.datasource.MovieDataSource
import com.example.movieapptest.data.db.AppDatabase
import com.example.movieapptest.data.model.Movie
import com.example.movieapptest.data.network.services.MovieService
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class PopularRepositoryImpl(private val service : MovieService,
                            private val db : AppDatabase) : PopularRepository, MovieDataSource {
    override fun save(list: List<Movie>) {
        db.movieDao().save(list)
    }

    override fun get(): MutableList<Movie>? = db.movieDao().get()

    override fun delete() {
        db.movieDao().delete()
    }

    override fun loadPopular(): Observable<PopularViewState> = service.loadPopular()
        .subscribeOn(Schedulers.io())
        .map<PopularViewState> {
            delete()
            save(it.results)
            PopularViewState.SuccessPopularListState(get() ?: emptyList())
        }
        .startWith(PopularViewState.Loading)
        .onErrorReturn {
            if(get().isNullOrEmpty()){
                PopularViewState.FailPopularListState(it.localizedMessage ?: "Unknown Error.")
            } else{
                PopularViewState.SuccessPopularListState(get() ?: emptyList())
            }
        }
}