package com.example.movieapptest.upcoming

import com.example.movieapptest.data.datasource.MovieDataSource
import com.example.movieapptest.data.db.AppDatabase
import com.example.movieapptest.data.model.Movie
import com.example.movieapptest.data.network.services.MovieService
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class UpcomingRepositoryImpl(private val service: MovieService,
                             private val db : AppDatabase) : UpcomingRepository, MovieDataSource {
    override fun save(list: List<Movie>) {
        db.movieDao().save(list)
    }

    override fun get(): MutableList<Movie>? = db.movieDao().get()

    override fun delete() {
        db.movieDao().delete()
    }

    override fun loadUpcoming(): Observable<UpcomingViewState> = service.loadUpcoming()
        .subscribeOn(Schedulers.io())
        .map<UpcomingViewState> {
            delete()
            save(it.results)
            UpcomingViewState.SuccessUpcomingListState(get() ?: emptyList())
        }
        .startWith(UpcomingViewState.Loading)
        .onErrorReturn {
            if(get().isNullOrEmpty()){
                UpcomingViewState.FailUpcomingListState(it.localizedMessage ?: "Unknown Error.")
            }else{
                UpcomingViewState.SuccessUpcomingListState(get() ?: emptyList())
            }
        }
}